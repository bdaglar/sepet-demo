using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using YemekSepetiDemoProject.Controllers;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Tests.Controllers
{
    [TestFixture]
    public class ItemControllerTest
    {
        private ItemController _controller;

        #region SetUp / TearDown

        [SetUp]
        public void Init()
        {
            var mockDAO = new Mock<IDAO<Item>>();
            mockDAO.Setup(dao => dao.Get(1)).Returns(new Item { Id = 1, Name = "item1", Price = 1.99, RestaurantId = 1});
            mockDAO.Setup(dao => dao.Get(It.Is<Int64>(id => id != 1))).Throws<ResourceNotFoundException>();
            mockDAO.Setup(dao => dao.Create(It.Is<Item>(i => !String.IsNullOrWhiteSpace(i.Name)))).Returns(1);
            mockDAO.Setup(dao => dao.Create(It.Is<Item>(i => String.IsNullOrWhiteSpace(i.Name)))).Throws<BadArgumentException>();
            mockDAO.Setup(dao => dao.Update(It.Is<Item>(i => !String.IsNullOrWhiteSpace(i.Name)))).Returns(true);
            mockDAO.Setup(dao => dao.Update(It.Is<Item>(i => String.IsNullOrWhiteSpace(i.Name)))).Throws<BadArgumentException>();
            mockDAO.Setup(dao => dao.Delete(It.IsAny<Int64>())).Returns(true);


            _controller = new ItemController(mockDAO.Object);
            _controller.Request = new HttpRequestMessage();
            _controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

        }

        [TearDown]
        public void Dispose()
        { }

        #endregion

        #region Tests

        [Test]
        public void Get_ShouldReturn200_WhenExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.OK;

            HttpResponseMessage response = _controller.Get(1);

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Get_ShouldReturn404_WhenNonExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.NotFound;

            HttpResponseMessage response = _controller.Get(5);

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Post_ShouldReturn400_WhenRequiredPropertiesMissing()
        {
            const HttpStatusCode expected = HttpStatusCode.BadRequest;

            HttpResponseMessage response = _controller.Post(new Item{ Price = 1.99, RestaurantId = 1 });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Post_ShouldReturn201_WhenRequiredPropertiesProvided()
        {
            const HttpStatusCode expected = HttpStatusCode.Created;

            HttpResponseMessage response = _controller.Post(new Item { Name = "item1", Price = 1.99, RestaurantId = 1 });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn200_WhenRequiredPropertiesAndExistingIdProvided()
        {
            const HttpStatusCode expected = HttpStatusCode.OK;

            HttpResponseMessage response = _controller.Put(1, new Item { Name = "item1", Price = 1.99, RestaurantId = 1 });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn400_WhenPropertiesMissing()
        {
            const HttpStatusCode expected = HttpStatusCode.BadRequest;

            HttpResponseMessage response = _controller.Put(1, new Item{ Price = 1.99, RestaurantId = 1 });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn404_WhenNonExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.NotFound;

            HttpResponseMessage response = _controller.Put(5, new Item { Name = "item1", Price = 1.99, RestaurantId = 1 });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn409_WhenConflictingIdsGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.Conflict;

            HttpResponseMessage response = _controller.Put(5, new Item { Id = 1, Name = "item1", Price = 1.99, RestaurantId = 1 });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Delete_ShouldReturn404_WhenNonExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.NotFound;

            HttpResponseMessage response = _controller.Delete(5);

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Delete_ShouldReturn200__WhenExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.OK;

            HttpResponseMessage response = _controller.Delete(1);

            response.StatusCode.Should().Be(expected);
        }

        #endregion
    }
}
