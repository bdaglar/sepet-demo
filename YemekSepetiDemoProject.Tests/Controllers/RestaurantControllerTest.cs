using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using YemekSepetiDemoProject.Controllers;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Tests.Controllers
{
    [TestFixture]
    public class RestaurantControllerTest
    {
        private RestaurantController _controller;
        #region SetUp / TearDown

        [SetUp]
        public void Init()
        {
            var mockDAO = new Mock<IDAO<Restaurant>>();
            mockDAO.Setup(dao => dao.Get(1)).Returns(new Restaurant { Id = 1, Name = "restaurant1" });
            mockDAO.Setup(dao => dao.Get(It.Is<Int64>(id => id != 1))).Throws<ResourceNotFoundException>();
            mockDAO.Setup(dao => dao.Create(It.Is<Restaurant>(r => !String.IsNullOrWhiteSpace(r.Name)))).Returns(1);
            mockDAO.Setup(dao => dao.Create(It.Is<Restaurant>(r => String.IsNullOrWhiteSpace(r.Name)))).Throws<BadArgumentException>();
            mockDAO.Setup(dao => dao.Update(It.Is<Restaurant>(r => !String.IsNullOrWhiteSpace(r.Name)))).Returns(true);
            mockDAO.Setup(dao => dao.Update(It.Is<Restaurant>(r => String.IsNullOrWhiteSpace(r.Name)))).Throws<BadArgumentException>();
            mockDAO.Setup(dao => dao.Delete(It.IsAny<Int64>())).Returns(true);


            _controller = new RestaurantController(mockDAO.Object);
            _controller.Request = new HttpRequestMessage();
            _controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TearDown]
        public void Dispose()
        { }

        #endregion

        #region Tests

        [Test]
        public void Get_ShouldReturn200_WhenExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.OK;

            HttpResponseMessage response = _controller.Get(1);

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Get_ShouldReturn404_WhenNonExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.NotFound;

            HttpResponseMessage response = _controller.Get(5);

            response.StatusCode.Should().Be(expected);
        }


        [Test]
        public void Post_ShouldReturn400_WhenRequiredPropertiesMissing()
        {
            const HttpStatusCode expected = HttpStatusCode.BadRequest;

            HttpResponseMessage response = _controller.Post(new Restaurant());

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Post_ShouldReturn201_WhenRequiredPropertiesProvided()
        {
            const HttpStatusCode expected = HttpStatusCode.Created;

            HttpResponseMessage response = _controller.Post(new Restaurant { Name = "restaurant1" });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn200_WhenRequiredPropertiesAndExistingIdProvided()
        {
            const HttpStatusCode expected = HttpStatusCode.OK;

            HttpResponseMessage response = _controller.Put(1, new Restaurant { Name = "restaurant1" });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn400_WhenPropertiesMissing()
        {
            const HttpStatusCode expected = HttpStatusCode.BadRequest;

            HttpResponseMessage response = _controller.Put(1, new Restaurant());

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn404_WhenNonExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.NotFound;

            HttpResponseMessage response = _controller.Put(5, new Restaurant { Name = "restaurant1" });

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Put_ShouldReturn409_WhenConflictingIdsGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.Conflict;

            HttpResponseMessage response = _controller.Put(5, new Restaurant { Id = 1, Name = "restaurant1" });

            response.StatusCode.Should().Be(expected);
        }


        [Test]
        public void Delete_ShouldReturn404_WhenNonExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.NotFound;

            HttpResponseMessage response = _controller.Delete(5);

            response.StatusCode.Should().Be(expected);
        }

        [Test]
        public void Delete_ShouldReturn200__WhenExistingIdGiven()
        {
            const HttpStatusCode expected = HttpStatusCode.OK;

            HttpResponseMessage response = _controller.Delete(1);

            response.StatusCode.Should().Be(expected);
        }

        #endregion
    }
}
