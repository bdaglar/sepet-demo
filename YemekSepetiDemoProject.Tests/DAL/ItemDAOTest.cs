﻿using System;
using FluentAssertions;
using NUnit.Framework;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

// Integration Tests For ItemDAO class
namespace YemekSepetiDemoProject.Tests.DAL
{
    [TestFixture]
    public class ItemDAOTest
    {

        [Test]
        public void Get_ShouldGetItem_WhenValidIdGiven()
        {
            Item expected;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
                expected = new Item{Id = 1, Name = "item1", Price = 1.00, RestaurantId = 1};
                db.Insert(new Restaurant{ Id = 1, Name = "restaurant1" });
                db.Insert(expected);
            }
      
            var dao = new ItemDAO(dbFactory);
            Item result = dao.Get(1);
            result.ShouldHave().AllProperties().EqualTo(expected);
        }

        [Test]
        public void Get_ShouldThrowNotFoundException_WhenInvalidIdGiven()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
                var expected = new Item { Id = 1, Name = "item1", Price = 1.00, RestaurantId = 1 };
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1" });
                db.Insert(expected);
            }

            var dao = new ItemDAO(dbFactory);

            Action act = () => dao.Get(2);
            act.ShouldThrow<ResourceNotFoundException>();
        }

        [Test]
        public void Create_ShouldReturnItemId_WhenRequiredPropertiesProvided()
        {
            const long expected = 1;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
            }

            var dao = new ItemDAO(dbFactory);

            long result = dao.Create(new Item { Name = "item1", Price = 1.00, RestaurantId = 1 });
            result.Should().Be(expected);
        }

        [Test]
        public void Create_ShouldThrowBadArgumentException_WhenPropertiesMissing()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
            }

            var dao = new ItemDAO(dbFactory);

            Action act = () => dao.Create(new Item { RestaurantId = 1 });
            act.ShouldThrow<BadArgumentException>();
        }

        [Test]
        public void Delete_ShouldReturnTrue_WhenValidIdGiven()
        {
            const bool expected = true;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.Insert(new Item { Id = 1, Name = "item1", Price = 1.00, RestaurantId = 1 });
            }

            var dao = new ItemDAO(dbFactory);
            Boolean result = dao.Delete(1);
            result.Should().Be(expected);
        }


        [Test]
        public void Update_ShouldReturnTrue_WhenRequiredPropertiesProvided()
        {
            const bool expected = true;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.Insert(new Item { Id = 1, Name = "item1", Price = 1.00, RestaurantId = 1 });
            }

            var dao = new ItemDAO(dbFactory);
            Boolean result = dao.Update(new Item { Id = 1, Name = "item2", Price = 2.00, RestaurantId = 1 });
            result.Should().Be(expected);
        }

        [Test]
        public void Update_ShouldThrowBadArgumentException_WhenPropertiesMissing()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.Insert(new Item { Id = 1, Name = "item1", Price = 1.99, RestaurantId = 1});
            }

            var dao = new ItemDAO(dbFactory);

            Action act = () => dao.Update(new Item { Id = 1 });
            act.ShouldThrow<BadArgumentException>();
        }

    }
}
