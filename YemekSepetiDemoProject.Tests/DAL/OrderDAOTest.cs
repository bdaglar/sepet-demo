using System;
using FluentAssertions;
using NUnit.Framework;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

// Integration Tests For ItemDAO class
namespace YemekSepetiDemoProject.Tests.DAL
{
    [TestFixture]
    public class OrderDAOTest
    {
        #region SetUp / TearDown

        [SetUp]
        public void Init()
        { }

        [TearDown]
        public void Dispose()
        { }

        #endregion

        #region Tests

        [Test]
        public void Get_ShouldGetOrder_WhenValidIdGiven()
        {
            Order expected;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
                db.CreateTable<Order>();
                expected = new Order {Id = 1, ItemId = 1, Quantity = 1, UserId = 1};
                db.Insert(new Item { Id = 1, Name = "item1", Price = 1.00, RestaurantId = 1 });
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1" });
                db.Insert(expected);
            }

            var dao = new OrderDAO(dbFactory);
            Order result = dao.Get(1);
            result.ShouldHave().AllProperties().EqualTo(expected);
        }

        [Test]
        public void Get_ShouldThrowNotFoundException_WhenInvalidIdGiven()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
                db.CreateTable<Order>();
                db.CreateTable<User>();
                var expected = new Order { Id = 1, ItemId = 1, Quantity = 1, UserId = 1 };
                db.Insert(new Item { Id = 1, Name = "item1", Price = 1.00, RestaurantId = 1 });
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1" });
                db.Insert(new User { Id = 1, Name = "user1" });
                db.Insert(expected);
            }

            var dao = new ItemDAO(dbFactory);

            Action act = () => dao.Get(2);
            act.ShouldThrow<ResourceNotFoundException>();
        }

        [Test]
        public void Create_ShouldReturnOrderId_WhenRequiredPropertiesProvided()
        {
            const long expected = 1;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Item>();
                db.CreateTable<Restaurant>();
                db.CreateTable<Order>();
            }

            var dao = new OrderDAO(dbFactory);

            long result = dao.Create(new Order { ItemId = 1, Quantity = 1, UserId = 1});
            result.Should().Be(expected);
        }

        [Test]
        public void Delete_ShouldReturnTrue_WhenValidIdGiven()
        {
            const bool expected = true;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Order>();
                db.Insert(new Order { Id = 1, ItemId = 1, Quantity = 1, UserId = 1 });
            }

            var dao = new OrderDAO(dbFactory);

            Boolean result = dao.Delete(1);
            result.Should().Be(expected);
        }

        [Test]
        public void Update_ShouldReturnTrue_WhenExistingIdGiven()
        {
            const bool expected = true;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Order>();
                db.Insert(new Order { Id = 1, ItemId = 1, Quantity = 1, UserId = 1 });
            }

            var dao = new OrderDAO(dbFactory);
            Boolean result = dao.Update(new Order { Id = 1, ItemId = 1, Quantity = 7, UserId = 1 });
            result.Should().Be(expected);
        }
        #endregion Tests
    }
}
