using System;
using FluentAssertions;
using NUnit.Framework;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

// Integration Tests For ItemDAO class
namespace YemekSepetiDemoProject.Tests.DAL
{
    [TestFixture]
    public class RestaurantDAOTest
    {
        #region SetUp / TearDown

        [SetUp]
        public void Init()
        { }

        [TearDown]
        public void Dispose()
        { }

        #endregion

        #region Tests

        [Test]
        public void Get_ShouldGetRestaurant_WhenValidIdGiven()
        {
            Restaurant expected;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
                expected = new Restaurant{ Id = 1, Name = "restaurant1" };
                db.Insert(expected);
            }

            var dao = new RestaurantDAO(dbFactory);
            Restaurant result = dao.Get(1);
            result.ShouldHave().AllProperties().EqualTo(expected);
        }

        [Test]
        public void Get_ShouldThrowNotFoundException_WhenInvalidIdGiven()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
                var expected = new Restaurant { Id = 1, Name = "restaurant1" };
                db.Insert(expected);
            }

            var dao = new RestaurantDAO(dbFactory);
            Action act = () => dao.Get(2);
            act.ShouldThrow<ResourceNotFoundException>();
        }

        [Test]
        public void Create_ShouldReturnRestaurantId_WhenRequiredPropertiesProvided()
        {
            const long expected = 1;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
            }

            var dao = new RestaurantDAO(dbFactory);

            long result = dao.Create(new Restaurant { Name = "restaurant1" });
            result.Should().Be(expected);
        }

        [Test]
        public void Create_ShouldThrowBadArgumentException_WhenPropertiesMissing()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
            }

            var dao = new RestaurantDAO(dbFactory);

            Action act = () => dao.Create(new Restaurant());
            act.ShouldThrow<BadArgumentException>();
        }

        [Test]
        public void Delete_ShouldReturnTrue_WhenValidIdGiven()
        {
            const bool expected = true;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1" });
            }

            var dao = new RestaurantDAO(dbFactory);

            Boolean result = dao.Delete(1);
            result.Should().Be(expected);
        }


        [Test]
        public void Update_ShouldReturnTrue_WhenExistingIdGiven()
        {
            const bool expected = true;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1" });
            }

            var dao = new RestaurantDAO(dbFactory);
            Boolean result = dao.Update(new Restaurant { Id = 1, Name = "restaurant5" });
            result.Should().Be(expected);
        }

        [Test]
        public void Update_ShouldThrowBadArgumentException_WhenPropertiesMissing()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<Restaurant>();
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1"});
            }

            var dao = new RestaurantDAO(dbFactory);

            Action act = () => dao.Update(new Restaurant { Id = 1 });
            act.ShouldThrow<BadArgumentException>();
        }

        #endregion
    }
}
