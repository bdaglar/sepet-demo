﻿using System;
using FluentAssertions;
using NUnit.Framework;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

// Integration Tests For ItemDAO class
namespace YemekSepetiDemoProject.Tests.DAL
{
    [TestFixture]
    public class UserDAOTest
    {
        #region SetUp / TearDown

        [SetUp]
        public void Init()
        { }

        [TearDown]
        public void Dispose()
        { }

        #endregion

        #region Tests

        [Test]
        public void Get_ShouldGetUser_WhenValidIdGiven()
        {
            User expected;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
                expected = new User { Id = 1, Name = "user1" };
                db.Insert(expected);
            }

            var dao = new UserDAO(dbFactory);
            User result = dao.Get(1);
            result.ShouldHave().AllProperties().EqualTo(expected);
        }

        [Test]
        public void Get_ShouldThrowNotFoundException_WhenInvalidIdGiven()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
                var expected = new User { Id = 1, Name = "user1" };
                db.Insert(expected);
            }

            var dao = new UserDAO(dbFactory);

            Action act = () => dao.Get(2);
            act.ShouldThrow<ResourceNotFoundException>();
        }

        [Test]
        public void Create_ShouldReturnUserId_WhenRequiredPropertiesProvided()
        {
            const long expected = 1;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
            }

            var dao = new UserDAO(dbFactory);

            long result = dao.Create(new User { Name = "user1" });
            result.Should().Be(expected);
        }

        [Test]
        public void Create_ShouldThrowBadArgumentException_WhenPropertiesMissing()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
            }

            var dao = new UserDAO(dbFactory);

            Action act = () => dao.Create(new User());
            act.ShouldThrow<BadArgumentException>();
        }

        [Test]
        public void Delete_ShouldReturnTrue_WhenValidIdGiven()
        {
            const bool expected = true;

            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
                db.Insert(new User { Id = 1, Name = "user1" });
            }

            var dao = new UserDAO(dbFactory);

            Boolean result = dao.Delete(1);
            result.Should().Be(expected);
        }

        [Test]
        public void Update_ShouldReturnTrue_WhenRequiredPropertiesProvided()
        {
            const bool expected = true;
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
                db.Insert(new User { Id = 1, Name = "user1" });
            }

            var dao = new UserDAO(dbFactory);
            Boolean result = dao.Update(new User { Id = 1, Name = "user5" });
            result.Should().Be(expected);
        }

        [Test]
        public void Update_ShouldThrowBadArgumentException_WhenPropertiesMissing()
        {
            var dbFactory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (var db = dbFactory.OpenDbConnection())
            {
                db.CreateTable<User>();
                db.Insert(new User { Id = 1, Name = "user1" });
            }

            var dao = new UserDAO(dbFactory);
            
            Action act = () => dao.Update(new User { Id = 1 });
            act.ShouldThrow<BadArgumentException>();
        }

        #endregion
    }
}
