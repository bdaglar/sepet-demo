﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ninject;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Controllers
{
    public class ItemController : ApiController
    {
        private readonly IDAO<Item> _itemDAO;

        [Inject]
        public ItemController(IDAO<Item> itemDAO)
        {
            _itemDAO = itemDAO;
        }

        // GET api/item/5
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var item = _itemDAO.Get(id);
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // POST api/item
        public HttpResponseMessage Post(Item item)
        {
            try
            {
                Int64 idn = _itemDAO.Create(item);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, item);
                String uri = Url.Route("DefaultApi", new { httproute = "", controller = "Item", id = idn });
                if (uri != null) response.Headers.Location = new Uri(uri);
                return response;
            }
            catch (BadArgumentException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // PUT api/item/5
        public HttpResponseMessage Put(int id, Item item)
        {
            try
            {
                if (item.Id != 0 && id != item.Id) return Request.CreateResponse(HttpStatusCode.Conflict);
                _itemDAO.Get(id);
                item.Id = id;
                _itemDAO.Update(item);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, item);
                return response;
            }
            catch (BadArgumentException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // DELETE api/item/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _itemDAO.Get(id);
                _itemDAO.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

    }
}
