﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ninject;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Controllers
{
    public class OrderController : ApiController
    {
        private readonly IDAO<Order> _orderDAO;

        [Inject]
        public OrderController(IDAO<Order> orderDAO)
        {
            _orderDAO = orderDAO;
        }

        // GET api/order/5
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var restaurant = _orderDAO.Get(id);
                return Request.CreateResponse(HttpStatusCode.OK, restaurant);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // POST api/order
        public HttpResponseMessage Post(Order order)
        {
            Int64 idn = _orderDAO.Create(order);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, order);
            String uri = Url.Route("DefaultApi", new { httproute = "", controller = "Order", id = idn });
            if (uri != null) response.Headers.Location = new Uri(uri);
            return response;
        }

        // PUT api/order/5
        public HttpResponseMessage Put(int id, Order order)
        {
            try
            {
                if (order.Id != 0 && id != order.Id) return Request.CreateResponse(HttpStatusCode.Conflict);
                _orderDAO.Get(id);
                order.Id = id;
                _orderDAO.Update(order);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, order);
                return response;
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // DELETE api/order/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _orderDAO.Get(id);
                _orderDAO.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }
    }
}
