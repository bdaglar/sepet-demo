﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ninject;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Controllers
{
    public class RestaurantController : ApiController
    {
        private readonly IDAO<Restaurant> _restaurantDAO;

        [Inject]
        public RestaurantController(IDAO<Restaurant> restaurantDAO)
        {
            _restaurantDAO = restaurantDAO;
        }

        // GET api/restaurant/5
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var restaurant = _restaurantDAO.Get(id);
                return Request.CreateResponse(HttpStatusCode.OK, restaurant);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // POST api/restaurant
        public HttpResponseMessage Post(Restaurant restaurant)
        {
            try
            {
                Int64 idn = _restaurantDAO.Create(restaurant);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, restaurant);
                String uri = Url.Route("DefaultApi", new { httproute = "", controller = "Restaurant", id = idn });
                if (uri != null) response.Headers.Location = new Uri(uri);
                return response;
            }
            catch (BadArgumentException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // PUT api/restaurant/5
        public HttpResponseMessage Put(int id, Restaurant restaurant)
        {
            try
            {
                if (restaurant.Id != 0 && id != restaurant.Id) return Request.CreateResponse(HttpStatusCode.Conflict);
                _restaurantDAO.Get(id);
                restaurant.Id = id;
                _restaurantDAO.Update(restaurant);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, restaurant);
                return response;
            }
            catch (BadArgumentException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // DELETE api/restaurant/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _restaurantDAO.Get(id);
                _restaurantDAO.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

    }
}
