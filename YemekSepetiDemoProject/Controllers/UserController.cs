﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ninject;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Controllers
{
    public class UserController : ApiController
    {
        private readonly IDAO<User> _userDAO;

        [Inject]
        public UserController(IDAO<User> userDAO)
        {
            _userDAO = userDAO;
        }

        // GET api/user/5
        public HttpResponseMessage Get(int id)
        {
            try
            {
                var user = _userDAO.Get(id);
                return Request.CreateResponse(HttpStatusCode.OK, user);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // POST api/user
        public HttpResponseMessage Post(User user)
        {
            try
            {
                Int64 idn  = _userDAO.Create(user);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, user);
                String uri = Url.Route("DefaultApi", new { httproute = "", controller = "User", id = idn });
                if (uri != null) response.Headers.Location = new Uri(uri);
                return response;
            }
            catch (BadArgumentException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // PUT api/user/5
        public HttpResponseMessage Put(int id, User user)
        {
            try
            {
                if( user.Id != 0 && id != user.Id ) return Request.CreateResponse(HttpStatusCode.Conflict);
                _userDAO.Get(id);
                user.Id = id;
                _userDAO.Update(user);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
                return response;
            }
            catch (BadArgumentException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

        // DELETE api/user/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _userDAO.Get(id);
                _userDAO.Delete(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (ResourceNotFoundException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

    }
}
