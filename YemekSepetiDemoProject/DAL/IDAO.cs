﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YemekSepetiDemoProject.DAL
{
    public interface IDAO<T>
    {
        T Get(Int64 id);
        Int64 Create(T entity);
        Boolean Delete(Int64 id);
        Boolean Update(T entity);
    }
}
