﻿using System;
using System.Data;
using System.Data.SQLite;
using Ninject;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.DAL
{
    public class ItemDAO : IDAO<Item>
    {
        private readonly IDbConnectionFactory _dbFactory;

        [Inject]
        public ItemDAO(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public Item Get(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    return db.GetById<Item>(id);
                }
                catch (ArgumentNullException e)
                {
                    throw new ResourceNotFoundException(e.Message);
                }
            }
        }

        public Int64 Create(Item item)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    db.Insert(item);
                    long lastInsertId = db.GetLastInsertId();
                    return lastInsertId;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }
        }

        public Boolean Delete(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                db.DeleteById<Item>(id);
                return true;
            }
        }

        public Boolean Update(Item item)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try{
                    db.Update(item, i => i.Id == item.Id);
                    return true;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }           
        }
    }
}