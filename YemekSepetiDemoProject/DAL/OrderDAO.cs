﻿using System;
using System.Data;
using System.Data.SQLite;
using Ninject;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.DAL
{
    public class OrderDAO : IDAO<Order>
    {
        private readonly IDbConnectionFactory _dbFactory;

        [Inject]
        public OrderDAO(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public Order Get(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    return db.GetById<Order>(id);
                }
                catch (ArgumentNullException e)
                {
                    throw new ResourceNotFoundException(e.Message);
                }
            }
        }

        public Int64 Create(Order order)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    db.Insert(order);
                    long lastInsertId = db.GetLastInsertId();
                    return lastInsertId;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }
        }

        public Boolean Delete(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                db.DeleteById<Order>(id);
                return true;
            }
        }

        public Boolean Update(Order order)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    db.Update(order, i => i.Id == order.Id);
                    return true;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }
        }
    }
}