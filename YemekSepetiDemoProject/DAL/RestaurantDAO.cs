﻿using System;
using System.Data;
using System.Data.SQLite;
using Ninject;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.DAL
{
    public class RestaurantDAO : IDAO<Restaurant>
    {
        private readonly IDbConnectionFactory _dbFactory;

        [Inject]
        public RestaurantDAO(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public Restaurant Get(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    return db.GetById<Restaurant>(id);
                }
                catch (ArgumentNullException e)
                {
                    throw new ResourceNotFoundException(e.Message);
                }
            }
        }

        public Int64 Create(Restaurant restaurant)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    db.Insert(restaurant);
                    long lastInsertId = db.GetLastInsertId();
                    return lastInsertId;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }
        }

        public Boolean Delete(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                db.DeleteById<Restaurant>(id);
                return true;
            }
        }

        public Boolean Update(Restaurant restaurant)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try{
                    db.Update(restaurant, i => i.Id == restaurant.Id);
                    return true;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }
        }
    }
}