﻿using System;
using System.Data;
using System.Data.SQLite;
using Ninject;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.Exceptions;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.DAL
{
    public class UserDAO : IDAO<User>
    {

        private readonly IDbConnectionFactory _dbFactory;

        [Inject]
        public UserDAO(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public User Get(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    return db.GetById<User>(id);
                }
                catch (ArgumentNullException e)
                {
                    throw new ResourceNotFoundException(e.Message);
                }
            }
        }


        public Int64 Create(User user)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    db.Insert(user);
                    long lastInsertId = db.GetLastInsertId();
                    return lastInsertId;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }
        }

        public Boolean Delete(Int64 id)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                db.DeleteById<User>(id);
                return true;
            }
        }

        public Boolean Update(User user)
        {
            using (IDbConnection db = _dbFactory.OpenDbConnection())
            {
                try
                {
                    db.Update(user, i => i.Id == user.Id);
                    return true;
                }
                catch (SQLiteException e)
                {
                    throw new BadArgumentException(e.Message);
                }
            }    
        }

    }
}