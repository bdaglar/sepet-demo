﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YemekSepetiDemoProject.Exceptions
{
    public class BadArgumentException: Exception
    {
        public BadArgumentException(string message) : base(message)
        {
        }

        public BadArgumentException()
        {
        }

    }
}