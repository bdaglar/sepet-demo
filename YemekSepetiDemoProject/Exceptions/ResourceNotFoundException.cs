﻿using System;


namespace YemekSepetiDemoProject.Exceptions
{
    public class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException(string message) : base(message)
        {
        }

        public ResourceNotFoundException()
        {
        }

    }
}