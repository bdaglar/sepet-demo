﻿
using System;
using System.ComponentModel.DataAnnotations;
using ServiceStack.DataAnnotations;

namespace YemekSepetiDemoProject.Models
{
    public class Item
    {
        [AutoIncrement]
        public Int64 Id { get; set; }

        [References(typeof(Restaurant))]
        public Int64 RestaurantId { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        public Double Price { get; set; }

    }
}