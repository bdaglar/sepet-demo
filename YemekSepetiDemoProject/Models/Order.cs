﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ServiceStack.DataAnnotations;

namespace YemekSepetiDemoProject.Models
{
    public class Order
    {
        [AutoIncrement]
        public Int64 Id { get; set; }

        [References(typeof(Item))]
        public Int64 ItemId { get; set; }

        [References(typeof(User))]
        public Int64 UserId { get; set; }

        [Required]
        public Int64 Quantity { get; set; }
    }
}