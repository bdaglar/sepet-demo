﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ServiceStack.DataAnnotations;

namespace YemekSepetiDemoProject.Models
{
    public class Restaurant
    {
        [AutoIncrement]
        public Int64 Id { get; set; }

        [Required]
        public String Name { get; set; }

        public List<Item> Items { get; set; }

    }
}