﻿using System;
using System.ComponentModel.DataAnnotations;
using ServiceStack.DataAnnotations;

namespace YemekSepetiDemoProject.Models
{
    public class User
    {
        [AutoIncrement]
        public Int64 Id { get; set; }

        [Required]
        public String Name { get; set; }
    }
}