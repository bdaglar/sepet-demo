﻿using System.Data;
using Ninject.Activation;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Modules
{
    public class DbConnectionFactoryProvider : Provider<IDbConnectionFactory>
    {
        protected override IDbConnectionFactory CreateInstance(IContext context)
        {
            var factory = new OrmLiteConnectionFactory(":memory:", false, SqliteDialect.Provider);
            using (IDbConnection db = factory.OpenDbConnection())
            {
                db.CreateTable<User>();
                db.CreateTable<Restaurant>();
                db.CreateTable<Item>(); 
                db.CreateTable<Order>();
                db.Insert(new User{ Id = 1, Name = "user1" });
                db.Insert(new Restaurant { Id = 1, Name = "restaurant1" });
                db.Insert(new Item { Id = 1, Name = "item1", Price = 1.99, RestaurantId = 1});
                db.Insert(new Order { Id = 1, ItemId = 1, Quantity = 1, UserId = 1});
            }
            return factory;
        }
    }
}