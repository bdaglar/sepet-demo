﻿using System.Data;
using Ninject.Modules;
using ServiceStack.OrmLite;
using YemekSepetiDemoProject.DAL;
using YemekSepetiDemoProject.Models;

namespace YemekSepetiDemoProject.Modules
{
    public class PersistenceModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDbConnectionFactory>().ToProvider<DbConnectionFactoryProvider>().InSingletonScope();
            Bind<IDAO<User>>().To<UserDAO>();
            Bind<IDAO<Order>>().To<OrderDAO>();
            Bind<IDAO<Restaurant>>().To<RestaurantDAO>();
            Bind<IDAO<Item>>().To<ItemDAO>();

        }
    }
}